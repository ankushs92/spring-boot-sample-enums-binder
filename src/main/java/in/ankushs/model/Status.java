package in.ankushs.model;

import in.ankushs.utils.Strings;

public enum Status {
	ACTIVE,
	DEACTIVE;
	
	public static Status from(String val){
		if(!Strings.hasText(val)){
			return null;
		}
		val = val.toLowerCase();
		if(val.equals("active")){
			return Status.ACTIVE;
		}
		else if(val.equals("deactive")){
			return Status.DEACTIVE;
		}
		else{
			throw new IllegalArgumentException("Invalid value.Status can only be 'active' or 'deactive'");
		}
	}
}
