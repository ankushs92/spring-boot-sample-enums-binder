package in.ankushs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootSampleCustomBinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootSampleCustomBinderApplication.class, args);
	}
}
