package in.ankushs.binder;

import java.beans.PropertyEditorSupport;

import in.ankushs.model.Status;
import in.ankushs.utils.Strings;

public class StatusBinder  extends PropertyEditorSupport{
	
	@Override
    public void setAsText(final String text) throws IllegalArgumentException {
		if(Strings.hasText(text)){
			final Status status = Status.from(text);
			setValue(status);
		}
		else{
			setValue(null);
		}
    }
}
