package in.ankushs.controller;

import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import in.ankushs.binder.StatusBinder;
import in.ankushs.model.Status;

@RestController
public class TestController {

	@InitBinder
	void initBinder(final WebDataBinder binder){
		binder.registerCustomEditor(Status.class, new StatusBinder());
	}
	
	@GetMapping("test")
	public String getStatus(@RequestParam("status") final Status status){
		return "status is : " + status;
	}

}
